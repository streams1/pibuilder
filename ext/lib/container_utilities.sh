#!/usr/bin/env bash
# vi :set ft-sh:

function permit_device_control() {
  local devices_mount_info
  local devices_subsytems
  local devices_subdir
  
  devices_mount_info=$(grep devices /proc/self/cgroup)

  if [ -z "$devices_mount_info" ]; then
    # cgroups not set up; must not be in a container
    return
  fi

  devices_subsytems=$(echo "${devices_mount_info}" | cut -d: -f2)
  devices_subdir=$(echo "${devices_mount_info}" | cut -d: -f3)

  if [ "$devices_subdir" = "/" ]; then
    # we're in the root devices cgroup; must not be in a container
    return
  fi

  cgroup_dir=/tmp/devices-cgroup

  if [ ! -e ${cgroup_dir} ]; then
    # mount our container's devices subsystem somewhere
    mkdir ${cgroup_dir}
  fi

  if ! mountpoint -q ${cgroup_dir}; then
    if ! mount -t cgroup -o "${devices_subsytems}" none "${cgroup_dir}"; then
      return 1
    fi
  fi

  # permit our cgroup to do everything with all devices
  # ignore failure in case something has already done this; echo appears to
  # return EINVAL, possibly because devices this affects are already in use
  echo a > "${cgroup_dir}${devices_subdir}/devices.allow" || true
}


function create_loop_devs() {
  for i in $(seq 64 67); do
    mknod -m 0660 "/scratch/loop${i}" b 7 "${i}"
    ln -s "/scratch/loop${i}" "/dev/loop${i}"
  done
}


function clear_loop_devs() {
  for i in $(seq 64 67); do
    losetup -d "/dev/loop${i}" > /dev/null 2>&1 || true
  done
}


assign_loop_device () {
  local dev offset file counter
  dev=$1
  offset=$2
  file=$3
  counter=10
  while (( counter>0 )) && ! losetup "${dev}" -o $offset "${file}" >/dev/null 2>&1; do
    progress_dot
    (( counter-- ))
  done
  [[ $counter -lt 10 ]] && printf '\n'
  return 0
}

