This is a project developed during my first stream sessions showing how using  combination of virtual machines and containers we can automate the development of Raspberry Pi images.

# Use

Create the working VM:

~~~bash
vagrant up
~~~

Log into VM usign `vagrant ssh`, then create the container to run a Raspberry Pi image:

~~~bash
sudo -i
cd /vagrant
docker build -t saltyvagrant/pibuilder .
~~~

Download and patch a Raspberry Pi image

~~~bash
curl -L https://downloads.raspberrypi.org/raspios_lite_armhf_latest -o filesystem.zip
unzip filesystem.zip
mv 2020-05-27-raspios-buster-lite-armhf.img filesystem.img
./pi_patch filesystem.img
~~~

Run the container referencing the patched image.

~~~bash
docker run --rm -it -v "$(pwd)":/sdcard saltyvagrant/pibuilder
~~~

You can now connect to the container and check out the `ssh` (assuming your runnig container's id starts `23`)

~~~
docker exec -it 23 /bin/sh

/ # ssh -y -y -i ./id_rsa pi@127.0.0.1 -p 5022
~~~

If all is well you should connect straight into the `pi` account on the emulated Pi.


