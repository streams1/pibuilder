FROM debian:buster-slim as dbbuild

RUN apt-get update \
    && apt-get install -y autoconf wget musl-tools gcc make bzip2 zlib1g-dev \
    && wget https://matt.ucc.asn.au/dropbear/releases/dropbear-2020.80.tar.bz2 \
    && tar xvf dropbear-2020.80.tar.bz2

WORKDIR dropbear-2020.80

RUN autoconf \
    && autoheader \
    && CC=musl-gcc ./configure --enable-static --disable-zlib \
    && make PROGRAMS="dbclient scp dropbearkey" \
    && chmod +x dbclient scp dropbearkey

FROM lukechilds/dockerpi:vm

COPY --from=dbbuild /dropbear-2020.80/dbclient /bin/ssh
COPY --from=dbbuild /dropbear-2020.80/scp /bin/scp
COPY --from=dbbuild /dropbear-2020.80/dropbearkey /bin/dropbearkey
COPY secure/id_rsa /id_rsa
